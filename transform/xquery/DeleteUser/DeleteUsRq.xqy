xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://Angelica/WebService/User";
(:: import schema at "../../../wsdl/usuario.wsdl" ::)
declare namespace ns2="http://Angelica/webservice/apiusers";
(:: import schema at "../../../wsdl/UsuarioBS.wsdl" ::)

declare variable $Input as element() (:: schema-element(ns1:deleteUserRequest) ::) external;

declare function local:func($Input as element() (:: schema-element(ns1:deleteUserRequest) ::)) as element() (:: schema-element(ns2:deleteUserRequest) ::) {
    <ns2:deleteUserRequest>
        <ns2:numDocument>{fn:data($Input/ns1:numDocument)}</ns2:numDocument>
    </ns2:deleteUserRequest>
};

local:func($Input)
