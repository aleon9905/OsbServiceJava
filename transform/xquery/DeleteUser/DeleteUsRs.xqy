xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://Angelica/WebService/User";
(:: import schema at "../../../wsdl/usuario.wsdl" ::)
declare namespace ns1="http://Angelica/webservice/apiusers";
(:: import schema at "../../../wsdl/UsuarioBS.wsdl" ::)

declare variable $Output as element() (:: schema-element(ns1:deleteUserResponse) ::) external;

declare function local:func($Output as element() (:: schema-element(ns1:deleteUserResponse) ::)) as element() (:: schema-element(ns2:deleteUserResponse) ::) {
    <ns2:deleteUserResponse>
        <ns2:code>{fn:data($Output/ns1:code)}</ns2:code>
        <ns2:message>{fn:data($Output/ns1:message)}</ns2:message>
    </ns2:deleteUserResponse>
};

local:func($Output)
