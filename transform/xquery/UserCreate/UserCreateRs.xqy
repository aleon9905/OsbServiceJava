xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://Angelica/WebService/User";
(:: import schema at "../../../wsdl/usuario.wsdl" ::)
declare namespace ns2="http://Angelica/webservice/apiusers";
(:: import schema at "../../../wsdl/UsuarioBS.wsdl" ::)

declare variable $Output as element() (:: schema-element(ns2:CreateUserResponse) ::) external;

declare function local:func($Output as element() (:: schema-element(ns2:CreateUserResponse) ::)) as element() (:: schema-element(ns1:UserCreateResponse) ::) {
    <ns1:UserCreateResponse>
        <ns1:code>{fn:data($Output/ns2:code)}</ns1:code>
        <ns1:message>{fn:data($Output/ns2:message)}</ns1:message>
    </ns1:UserCreateResponse>
};

local:func($Output)