xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://Angelica/WebService/User";
(:: import schema at "../../../wsdl/usuario.wsdl" ::)
declare namespace ns2="http://Angelica/webservice/apiusers";
(:: import schema at "../../../wsdl/UsuarioBS.wsdl" ::)

declare variable $input as element() (:: schema-element(ns1:CreateUserRequest) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:CreateUserRequest) ::)) as element() (:: schema-element(ns2:CreateUserRequest) ::) {
    <ns2:CreateUserRequest>
        <ns2:name>{fn:data($input/ns1:name)}</ns2:name>
        <ns2:typeDocument>{fn:data($input/ns1:typeDocument)}</ns2:typeDocument>
       <ns2:numDocument>{fn:data($input/ns1:numDocument)}</ns2:numDocument>
    </ns2:CreateUserRequest>
};

local:func($input)